import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { addCycleCount, setCycleCounts } from '../../actions/cycleCount';

import { cycleCounts } from '../fixtures/cycleCounts';

const createMockStore = configureMockStore([thunk]);

beforeEach(() => {

});

test('confirm addCycleCount action', () => {
  const result = addCycleCount(cycleCounts[0]);
  expect(result).toEqual({
    type: 'ADD_CYCLECOUNT',
    cycleCount: {
      id: 123,
    }
  })
});


test('confirm setCycleCounts action', () => {
  const result = setCycleCounts(cycleCounts);
  expect(result).toEqual({
    type: 'SET_CYCLECOUNTS',
    cycleCounts: [
      {
        id: 123,
      },
    ]
  })
});
