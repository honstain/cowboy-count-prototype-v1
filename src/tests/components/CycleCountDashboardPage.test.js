import React from 'react';
import { shallow } from 'enzyme';
import { CycleCountDashboardPage } from '../../components/CycleCountDashboardPage';

import { cycleCounts } from '../fixtures/cycleCounts';

test('should render empty dashboard page correctly', () => {
  const wrapper = shallow(<CycleCountDashboardPage cycleCountRecords={[]}/>);
  expect(wrapper).toMatchSnapshot();
});

test('should render dashboard page correctly', () => {
  const wrapper = shallow(<CycleCountDashboardPage cycleCountRecords={cycleCounts}/>);
  expect(wrapper).toMatchSnapshot();
});

