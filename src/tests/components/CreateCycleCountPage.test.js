import React from 'react';
import { shallow } from 'enzyme';
import { CreateCycleCountPage } from '../../components/CreateCycleCountPage';

test('should render empty dashboard page correctly', () => {
  const wrapper = shallow(<CreateCycleCountPage />);
  expect(wrapper).toMatchSnapshot();
});
