import todoReducer from '../../reducers/todo';


import { cycleCounts } from '../fixtures/cycleCounts';


test('should set default state', () => {
  const state = todoReducer(undefined, { type: '@@INIT' });
  expect(state).toEqual([]);
});

test('should add cycle count to existing state', () => {
  const state = todoReducer([], { type: 'ADD_CYCLECOUNT', cycleCount: cycleCounts[0] });
  expect(state).toEqual([cycleCounts[0]]);
});

test('should set cycle counts and replace state', () => {
  const state = todoReducer([], { type: 'SET_CYCLECOUNTS', cycleCounts: cycleCounts });
  expect(state).toEqual(cycleCounts);
});
