
export const addCycleCount = (cycleCount) => ({
  type: 'ADD_CYCLECOUNT',
  cycleCount,
});

export const addCycleCountAndStore = (cycleCount) => {
  return (dispatch) => {
    return fetch( 'http://localhost:8080',{
      method: 'POST',
      headers: { 'CONTENT-TYPE': 'application/json' },
      body: JSON.stringify(cycleCount),
    })
    .then(response => {
      console.log('CREATE');
    });
  }
}

export const setCycleCounts = (cycleCounts) => ({
  type: 'SET_CYCLECOUNTS',
  cycleCounts,
});