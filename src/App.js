import React from 'react';
import { Provider } from 'react-redux';

import AppRouter from './AppRouter';
import configureStore from './configureStore';

import 'normalize.css/normalize.css';
import './styles/styles.scss';

// TODO - Remove this once finished bootstrapping (will replace with external service call)
import { addCycleCount, setCycleCounts } from './actions/cycleCount';
import { cycleCounts } from './tests/fixtures/cycleCounts';

const store = configureStore();


fetch('http://localhost:8080')
  .then(response => response.json().then(body => ({ response, body })))
  .then(({response, body}) => {
    if (response.ok) {
      console.log(body);
      store.dispatch(setCycleCounts(body));
    } else {
      console.log('ERROR');
    }
  });

//store.dispatch(setCycleCounts(cycleCounts));

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <AppRouter />
      </Provider>
    </div>
  );
}

export default App;
