import React from 'react';
import { connect } from 'react-redux';

export const CycleCountDashboardPage = (props) => (

    <div className="content-container">
      <h3>Cycle Count Dashboard</h3>
      <div>
        {
          props.cycleCountRecords.map((record) =>
            <p key={record.id}>{record.id}</p>
          )
        }
      </div>
    </div>

);

const mapStateToProps = (state) => {
  return {
    cycleCountRecords: state,
  };
};

export default connect(mapStateToProps)(CycleCountDashboardPage);