import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import uuid from 'uuid';

import { addCycleCountAndStore } from '../actions/cycleCount';


export class CreateCycleCountPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: '',
      sku: '',
      qty: '',
    };
  };
  onLocationChange = (e) => {
    const location = e.target.value;
    this.setState(() => ({ location }));
  };
  onSKUChange = (e) => {
    const sku = e.target.value;
    this.setState(() => ({ sku }));
  };
  onQtyChange = (e) => {
    const qty = e.target.value;
    if (!qty || qty.match(/^\d{1,}$/)) {
      this.setState(() => ({ qty }));
    }
  }

  onSubmit = (e) => {
    e.preventDefault();
    const cycleCount = {
      id: uuid(),
      location: this.state.location,
      sku: this.state.sku,
      qty: this.state.qty,
      createdAt: moment().valueOf(),
    };
    this.props.addCycleCountAndStore(cycleCount);
    this.props.history.push('/');
  }
  render() {
    return (
      <div className="content-container">
        <h3>Create Cycle Count</h3>

        <form className="form" onSubmit={this.onSubmit}>
          <input
            type="text"
            placeholder="Location"
            className="text-input"
            value={this.state.location}
            onChange={this.onLocationChange}
          />
          <input
            type="text"
            placeholder="SKU"
            className="text-input"
            value={this.state.sky}
            onChange={this.onSKUChange}
          />
          <input
            type="number"
            placeholder="Quantity"
            className="text-input"
            value={this.state.qty}
            onChange={this.onQtyChange}
          />
          <div>
            <button className="button">Submit</button>
          </div>
        </form>
      </div>
    );
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addCycleCountAndStore: (cycleCount) => dispatch(addCycleCountAndStore(cycleCount)),
  };
};

export default connect(undefined, mapDispatchToProps)(CreateCycleCountPage);
