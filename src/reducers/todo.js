

const todoReducerDefaultState = [];

const todoReducer = (state = todoReducerDefaultState, action) => {
  switch (action.type) {
    case 'ADD_CYCLECOUNT':
      return [
        ...state,
        action.cycleCount,
      ];
    case 'SET_CYCLECOUNTS':
      return action.cycleCounts;
    default:
      return state;
  }
};

export default todoReducer;